module github.com/jonas747/discord-fs

go 1.16

require (
	github.com/hanwen/go-fuse v1.0.0
	github.com/hashicorp/golang-lru v0.5.4
	github.com/jonas747/discordgo v1.5.2
)
